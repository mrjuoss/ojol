// Illustrations
import WelcomeAuthImage from './illustrations/welcome.png';
import IllustrationRegister from './illustrations/register.svg';

// Icons
import IconBack from './icons/left-arrow.svg';

export { WelcomeAuthImage, IllustrationRegister, IconBack };