import React from 'react';
import {View, Text, Image} from 'react-native';

import ActionButton from './ActionButton';
import { colors } from '../../utils';
import { WelcomeAuthImage } from '../../assets';

const WelcomeAuth = ({navigation}) => {
  const handleGoTo = (screen) => {
    navigation.navigate(screen);
  }
  return (
    <View style={styles.wrapper.page}>
      <Image source={WelcomeAuthImage} style={styles.wrapper.illustration} />
      <Text style={styles.text.welcome}> Welcome O-JOL App </Text>
      <ActionButton
        desc="Please log in, if you already have an account"
        title="Login"
        onPress={() => handleGoTo('Login')}
      />
      <ActionButton
        desc="or please register if you don't have an account"
        title="Register"
        onPress={() => handleGoTo('Register')}
      />
    </View>
  );
};

const styles = {
  wrapper: {
    page: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'white'
    },
    illustration: {
      width: 219,
      height: 117,
      marginBottom: 8
    },
  },
  text: {
    welcome: {
      fontSize: 18,
      fontWeight: 'bold',
      color: colors.default,
      marginBottom: 76
    }
  }
}

export default WelcomeAuth;