import React from 'react';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Text } from 'react-native';
import { colors } from '../../../utils';

const Button = ({title, onPress}) => {
  return (
    <TouchableOpacity
      style={styles.wrapper.default}
      onPress={onPress}>
      <Text style={styles.text.default} >
        {title}
      </Text>
    </TouchableOpacity>
  )
}

const styles = {
  wrapper: {
    default: {
      backgroundColor: colors.default,
      borderRadius: 24,
      paddingVertical: 12
    },
  },
  text: {
    default: {
      fontSize: 12,
      fontWeight: 'bold',
      color: 'white',
      textTransform: 'uppercase',
      textAlign: 'center'
    }
  }
}

export default Button;