import React from 'react'
import { View, Text } from 'react-native'
import { TextInput } from 'react-native'
import { colors } from '../../../utils'

const Input = ({placeholder}) => {
  return (
    <View>
      <TextInput
      style={styles.input}
      placeholder={placeholder}
      placeholderTextColor={colors.default} />
    </View>
  )
}

const styles = {
  input: {
    borderWidth: 1,
    borderColor: colors.default,
    borderRadius: 8,
    marginBottom: 8,
    paddingHorizontal: 16,
    fontSize: 14,
    color: colors.default,
  }
}
export default Input;
