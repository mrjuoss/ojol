import React from 'react';

import { View, Text } from 'react-native'
import { colors } from '../../utils';
import { Input } from '../../components';
import { IconBack, IllustrationRegister } from '../../assets';

const Register = () => {
  return (
    <View style={styles.wrapper.page}>
      <IconBack
        width={25}
        height={25}
      />
      <IllustrationRegister
        width={106}
        height={115}
        style={styles.illustration}
      />
      <Text style={styles.text.desc}>
        Please accept data for the registration process.
      </Text>
      <View style={styles.space(40)} />
      <Input placeholder="Full Name"/>
      <Input placeholder="Email"/>
      <Input placeholder="Password"/>
    </View>
  )
}

const styles = {
  wrapper: {
    page: { margin: 16 },
  },
  illustration: {
    marginTop: 8
  },
  text: {
    desc: {
      fontSize: 14,
      fontWeight: 'bold',
      color: colors.default,
      marginTop: 16,
      maxWidth: 200
    }
  },
  space: (value) => {
    return {
      height: value
    }
  }
}
export default Register;